# RSQLite

This is a fork of [RSQLite](https://CRAN.r-project.org/package=RSQLite) R package. Fork is made on RSQLite v1.0.0 (2014-10-25), at the time when package was lightweight. No new features are being added here, it works good enough, and light enough. SQLite has been upgraded to 3.31.1 (2020-01-27). The only dependency is [DBI](https://CRAN.r-project.org/package=DBI) (== 0.3.1), provided together with RSQLite.

```r
install.packages(c("DBI","RSQLite"), repos="https://jangorecki.gitlab.io/rsqlite")
```

----

RSQLite embeds the SQLite database engine in R, providing a DBI-compliant interface. [SQLite](http://www.sqlite.org) is a public-domain, single-user, very light-weight database engine that implements a decent subset of the SQL 92 standard, including the core table creation, updating, insertion, and selection operations, plus transaction management.

## Basic usage

```R
library(DBI)
# Create an ephemeral in-memory RSQLite database
con <- dbConnect(RSQLite::SQLite(), ":memory:")

dbListTables(con)
dbWriteTable(con, "mtcars", mtcars)
dbListTables(con)

dbListFields(con, "mtcars")
dbReadTable(con, "mtcars")

# You can fetch all results:
res <- dbSendQuery(con, "SELECT * FROM mtcars WHERE cyl = 4")
dbFetch(res)
dbClearResult(res)

# Or a chunk at a time
res <- dbSendQuery(con, "SELECT * FROM mtcars WHERE cyl = 4")
while(!dbHasCompleted(res)){
  chunk <- dbFetch(res, n = 5)
  print(nrow(chunk))
}
# Clear the result
dbClearResult(res)

# Disconnect from the database
dbDisconnect(con)
```

## Acknowledgements

Many thanks to Doug Bates, Seth Falcon, Detlef Groth, Ronggui Huang, Kurt Hornik, Uwe Ligges, Charles Loboz, Duncan Murdoch, and Brian D. Ripley for comments, suggestions, bug reports, and/or patches.

## Update version of SQLite

1.  Download latest SQLite source

    ```R
    latest <- "http://sqlite.org/2014/sqlite-amalgamation-3080600.zip"
    tmp <- tempfile()
    download.file(latest, tmp)
    unzip(tmp, exdir = "src/sqlite", junkpaths = TRUE)
    unlink("src/sqlite/shell.c")
    ```
1.  Update `DESCRIPTION` for included version of SQLite

1.  Update `NEWS`

## Update datasets database

RSQLite includes one SQLite database (accessible from `datasetsDb()` that contains all data frames in the datasets package. This is the code that created it.

```R
tables <- unique(data(package = "datasets")$results[, 3])
tables <- tables[!grepl("(", tables, fixed = TRUE)]

con <- dbConnect(SQLite(), "inst/db/datasets.sqlite")
for(table in tables) {
  df <- getExportedValue("datasets", table)
  if (!is.data.frame(df)) next
  
  message("Creating table: ", table)
  dbWriteTable(con, table, as.data.frame(df), overwrite = TRUE)
}
```
